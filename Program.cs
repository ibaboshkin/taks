﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chess
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*****************************");
            Console.WriteLine("*                           *");
            Console.WriteLine("*  Welcome to Chess world!  *");
            Console.WriteLine("*                           *");
            Console.WriteLine("*****************************");

            bool correct = false;
            byte width = 0;
            byte height = 0;
            do
            {
                try
                {
                    Console.Write("\nEnter width: ");
                    width = byte.Parse(Console.ReadLine());
                    Console.Write("Enter height: ");
                    height = byte.Parse(Console.ReadLine());
                    correct = true;
                }
                catch (FormatException e)
                {
                    //Console.WriteLine(e.ToString());
                    Console.WriteLine("You must enter integer values! Try again, you stupid asshole!");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unknown exception: {0}\nTry again, you stupid asshole!", e.Message);
                }
            }while(!correct);

            string line = "";
            int i;
            for (i = 0; i < width; i++)
            {
                line += "* ";
            }

            for (i = 0; i < height; i++)
            {
                if (i % 2 == 0)
                    Console.WriteLine(line);
                else
                    Console.WriteLine(" " + line);
            }

            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey();
        }
    }
}
